Flappy times ahoy!
============
It's like that helicopter game that spawned a popular clone, which somehow made a ton of coin.

Documentation
-------------
There's a fair number of comments, and nothing else.

Other deets
-----------
Dependency management: require.js
DOM wizardry: jQuery
templates: jshaml
